var async = require('async'),
    debug = require('debug')('glass'),
    nopt = require('nopt'),
    fs = require('fs'),
    path = require('path'),
    matchme = require('matchme'),
    util = require('util'),
    AST = require('./ast'),
    _ = require('underscore'),
    
    cmdline = {
        known: {
            'silent': Boolean,
            'debug': Boolean,
            'target': path,
            'config': path,
            'variant': 'String'
        },
        
        shorthand: {}
    },
    
    detectors = _loadAll('detectors'),
    collectors = _loadAll('collectors'),
    tweakers = _loadAll('tweakers');
    
function glass(opts, callback) {
    // if we have no arguments, then check the command line args
    if (arguments.length === 0) {
        opts = nopt(cmdline.known, cmdline.shorthand, process.argv, 2);
    }
    else if (typeof opts == 'function') {
        callback = opts;
        opts = {};
    }
    
    // ensure we have options
    opts = opts || {};
    
    // ensure a callback is defined
    callback = callback || function(err) {
        if (err) {
            out('!{red}{0}', err);
        }
    };
    
    // if no target has been defined, but we have remaining arguments
    // then update the target
    if (!opts.target && opts.argv && opts.argv.remain.length) {
        opts.target = path.resolve(opts.argv.remain.join(' '));
    }

    // if we don't have a target, then throw an error
    if (! opts.target) return callback(new Error('no target specified'));
    
    // default the config to a glass.js file in the cwd
    opts.config = opts.config || path.resolve('glass.js');
    
    // default the variant to browser
    opts.variant = opts.variant || 'browser';
    
    // initialise the debug function
    debug = opts.debug ? require('out') : require('debug')('glass');
    
    // load the config, getting the full expanded options
    loadConfig(opts.config, opts, function(err, opts) {
        // process the target file
        process(opts.target, opts, callback);
    });
}

function loadConfig(configFile, opts, callback) {
    var configFiles = [
        path.resolve(__dirname, 'config', 'base.js'), // base config
        path.resolve(__dirname, 'config', opts.variant + '.js'), // variant config
        configFile
    ];
    
    // find the config files to load
    debug('locating config files');
    async.filter(configFiles, path.exists, function(existingFiles) {
        debug('found ' + existingFiles.length + ' valid config files');
        var allOpts = existingFiles.map(require).concat(opts),
            config = allOpts.reduce(function(memo, item) {
                // iterate through the item keys
                for (var key in item) {
                    if (Array.isArray(item[key]) || Array.isArray(memo[key])) {
                        memo[key] = _.union(memo[key] || [], item[key] || []);
                    }
                    else {
                        memo[key] = item[key];
                    }
                }
                
                return memo;
            }, {});
        
        callback(null, config);
    });
}

function process(target, opts, callback) {
    // if we are running in silent mode, replace out with a dummy function
    var out = opts.silent ? function() {} : require('out');
    
    // read the target file
    debug('reading ' + opts.target);
    fs.readFile(opts.target, 'utf8', function(err, data) {
        if (err) return callback(err);
        debug('parsing code');
        
        var ast = new AST(data, opts),
            pendingDetectors = [].concat(detectors),
            found;

        // if we are running in debug mode, write the AST json to a debug.json file
        if (opts.debug) {
            var debugFile = path.resolve('debug.json');
            
            out('writing: !{underline}{0}', debugFile);
            fs.writeFileSync(debugFile, JSON.stringify(ast.root, null, 2), 'utf8');
        }
        
        out('running detection');
        ast.traverse(function(node, path) {
            // run the detectors
            this.checkNode(node, path, pendingDetectors, function(detector, index) {
                // replace the detector with an undefined value (we've found it)
                pendingDetectors[index] = undefined;
            });
            
            // run the collector
            this.checkNode(node, path, collectors, function(collector, index) {
            });
            
            // run the tweakers
            this.checkNode(node, path, tweakers);
        });
        
        found = detectors.filter(function(detector, index) {
            return !pendingDetectors[index];
        });
        
        console.log(ast.toJSON());
        console.log(found);
    });
}

function _loadAll(folder) {
    var targetPath = path.resolve(__dirname, folder),
        items = [];
    
    fs.readdirSync(targetPath).forEach(function(file) {
        var item = items[items.length] = require('./' + folder + '/' + file);
            
        item.name = item.name || path.basename(file, '.js');
    });
    
    return items;
}

module.exports = glass;