/**
# Tweaker: Glass
The Glass tweaker is used to detect any calls to the `glass` utility functions. Depending on 
the packaging context these functions will be replaced with a platform suitable alternative.
*/
exports.conditions = ['type == CallExpression && callee.object.name == glass'];

exports.validate = function(node, path) {
    console.log(node);
};