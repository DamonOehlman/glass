var matchme = require('matchme'),
    esprima = require('esprima'),
    periscope = require('periscope'),
    vm = require('vm'),
    context = vm.createContext();

function AST(source, opts) {
    // ensure we have options
    opts = opts || {};
    
    // copy the opts across to this object
    for (var key in opts) {
        this[key] = opts[key];
    }

    // create the debug function
    this.debug = opts.debug ? require('out') : require('debug')('glass-ast');
    
    // initialise the module requirements for this module
    this._requires = {};
    
    this.root = esprima.parse(source, {
        range: true
    });

    // use periscope to perform scope analysis
    periscope(this.root);
}

// taken from esmorph: https://github.com/ariya/esmorph
AST.prototype.NodeType = {
    AssignmentExpression: 'AssignmentExpression',
    ArrayExpression: 'ArrayExpression',
    BlockStatement: 'BlockStatement',
    BinaryExpression: 'BinaryExpression',
    BreakStatement: 'BreakStatement',
    CallExpression: 'CallExpression',
    CatchClause: 'CatchClause',
    ConditionalExpression: 'ConditionalExpression',
    ContinueStatement: 'ContinueStatement',
    DoWhileStatement: 'DoWhileStatement',
    DebuggerStatement: 'DebuggerStatement',
    EmptyStatement: 'EmptyStatement',
    ExpressionStatement: 'ExpressionStatement',
    ForStatement: 'ForStatement',
    ForInStatement: 'ForInStatement',
    FunctionDeclaration: 'FunctionDeclaration',
    FunctionExpression: 'FunctionExpression',
    Identifier: 'Identifier',
    IfStatement: 'IfStatement',
    Literal: 'Literal',
    LabeledStatement: 'LabeledStatement',
    LogicalExpression: 'LogicalExpression',
    MemberExpression: 'MemberExpression',
    NewExpression: 'NewExpression',
    ObjectExpression: 'ObjectExpression',
    Program: 'Program',
    Property: 'Property',
    ReturnStatement: 'ReturnStatement',
    SequenceExpression: 'SequenceExpression',
    SwitchStatement: 'SwitchStatement',
    SwitchCase: 'SwitchCase',
    ThisExpression: 'ThisExpression',
    ThrowStatement: 'ThrowStatement',
    TryStatement: 'TryStatement',
    UnaryExpression: 'UnaryExpression',
    UpdateExpression: 'UpdateExpression',
    VariableDeclaration: 'VariableDeclaration',
    VariableDeclarator: 'VariableDeclarator',
    WhileStatement: 'WhileStatement',
    WithStatement: 'WithStatement'
};

AST.prototype.checkNode = function(node, path, rules, passFn) {
    // iterate through the rules
    for (var ruleIdx = rules.length; ruleIdx--; ) {
        var rule = rules[ruleIdx], match = true;
        
        // if the rule is valid, then check it
        if (rule) {
            // check the detector conditions first
            for (var ii = 0; match && rule.conditions && ii < rule.conditions.length; ii++) {
                match = match && matchme(node, rule.conditions[ii]);
            }
            
            // if we have a pass on the conditions (or we have no conditions)
            // then give the detector the opportunity to validate
            // if validation passes, then mark the detector as found and remove it from the pending detectors
            if (match && (typeof rule.validate == 'undefined' || rule.validate.call(this, node, path)) && passFn) {
                passFn.call(this, rule, ruleIdx);
            }
        }
    }
};

// TODO: speed this up a little
AST.prototype.isDefined = function(name, path) {
    var counter = 0,
        isGlobal = vm.runInContext('typeof ' + name, context) != 'undefined' || 
            (this.globals || []).indexOf(name) >= 0,
        isDefined = isGlobal;
        
    if (isGlobal) return true;
    
    while (!isDefined && path) {
        var node = path[0],
            ii;
            
        if (node.scope) {
            isDefined = node.scope.definitions.indexOf(name) >= 0;
            break;
        }

        path = path[1];
    }
    
    if (! isDefined) {
        this.debug('found unknown global: ' + name);
    }
    
    return isDefined;
};

AST.prototype.require = function(module, exportName) {
    var requires = this._requires[module];
    
    if (! requires) {
        requires = this._requires[module] = [];
    }
    
    if (! requires.indexOf) {
        console.log(module, exportName, this._requires);
    }
    
    if (exportName && requires.indexOf(exportName) < 0) {
        requires[requires.length] = exportName;
    }
};

// adapted from esmorph: https://github.com/ariya/esmorph
AST.prototype.traverse = function(object, visitor, master, depth) {
    var key, child, parent, path;

    // remap args if required
    if (typeof object == 'function') {
        visitor = object;
        object = this.root;
    }
    
    // ensure the depth is a numeric value
    depth = depth || 0;

    parent = (typeof master === 'undefined') ? [] : master;

    if (visitor.call(this, object, parent, depth) === false) {
        return;
    }
    for (key in object) {
        if (object.hasOwnProperty(key)) {
            child = object[key];
            path = [ object ];
            path.push(parent);
            if (typeof child === 'object' && child !== null) {
                this.traverse(child, visitor, path, depth + 1);
            }
        }
    }
};

AST.prototype.toJSON = function() {
    var output = {},
        ast = this;
    
    Object.keys(this).forEach(function(key) {
        if (key && key[0] === '_') {
            output[key.slice(1)] = ast[key];
        }
    });
    
    return output;
};

module.exports = AST;