/**
# Collector: Global Object
*/
exports.conditions = ['type == CallExpression && callee.object.name'];

exports.validate = function(node, path, depth) {
    var requiredFn = node.callee.property.name,
        requiredTarget = node.callee.object.name;

    // if the required function is any of the object prototype functions pass
    if (requiredFn in Object) return;
    
    // if the current property is not defined, then add the require
    if (this._requires[requiredTarget] || (!this.isDefined(requiredTarget, path, depth))) {
        this.require(requiredTarget, requiredFn);
    }
};