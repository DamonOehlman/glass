/**
# Collector: Global Function
*/
exports.conditions = ['type == CallExpression && callee.name'];

exports.validate = function(node, path) {
    var requiredTarget = node.callee.name;

    // if the current property is not defined, then add the require
    if (this._requires[requiredTarget] || (!this.isDefined(requiredTarget, path))) {
        this.require(requiredTarget);
    }
};