// define globals that won't be detected naturally in a VM context
exports.globals = [
    // interval and timeout functions don't register in a new vm context
    // so they are patched in here
    'setTimeout',
    'clearTimeout',
    'setInterval',
    'clearInterval'
];