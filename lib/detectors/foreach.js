exports.conditions = ['type == Identifier && name == forEach'];

exports.validate = function(node, path) {
    return path[0] && path[0].type == 'MemberExpression';
};