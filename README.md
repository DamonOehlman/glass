# Glass

Glass is a project designed to make programming for readability the highest priority.  Forget debates about semicolons, the sheer number of javascript module loader patterns out there, just code.

The primary goal of Glass is to make coding in JS as accessible and as safe as possible.  The project is made possible thanks to the excellent [esprima](https://github.com/ariya/esprima) parser.

## Features

The following are features of glass:

### Require Detection

To be completed.

### Shim Detection

To be completed.

